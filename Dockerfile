FROM node:11-alpine

RUN npm install -g dockerlint
 
CMD ["/usr/local/bin/dockerlint"]
